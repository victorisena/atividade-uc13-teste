﻿using System;

namespace UcPag
{
    /// <summary>
    /// Bank account demo class.
    /// </summary>
    /// 
   
    public class Pagamento
    {
        private double resultado;
        public static double GerarTaxaJuro()
        {
            return 0.01; 
        }

        public double Resultado
        {
            get { return resultado; }
        }

        public void CalculaJuros(double valor_inicial,int periodo)
        {
            if ( valor_inicial > 0 && periodo > 0)
            {
                resultado = Math.Round(valor_inicial * Math.Pow(1 + GerarTaxaJuro() , periodo),2);
              
            }
            else
            {
                throw new ArgumentOutOfRangeException("Zero em um dos parametros");
            }
            
        }


        public static void Main()
        {

        }
    }
}
