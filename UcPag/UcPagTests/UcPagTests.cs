using Microsoft.VisualStudio.TestTools.UnitTesting;
using UcPag;
namespace UcPagTests
{
    [TestClass]
    public class UcPagTests
    {
        [TestMethod]
        public void Verificando_a_taxa_de_juro()
        {
            var txjuro = Pagamento.GerarTaxaJuro();

            Assert.AreEqual(0.01, txjuro, "Erro na Gera��o da Taxa");
        }

        [TestMethod]
        public void Verificando_calculo_do_juro_composto()
        {
            Pagamento pagamento = new Pagamento();
            pagamento.CalculaJuros(100, 5);

            Assert.AreEqual(105.10, pagamento.Resultado, "Erro no calculo de Juro");
        }

        [TestMethod]
        public void Verificando_parametro_do_calculo_do_juro_composto_com_valor_inicial_igual_a_zero()
        {
            Pagamento pagamento = new Pagamento();
            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => pagamento.CalculaJuros(0, 5));
        }

        [TestMethod]
        public void Verificando_parametro_do_calculo_do_juro_composto_com_periodo_igual_a_zero()
        {
            Pagamento pagamento = new Pagamento();
            Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => pagamento.CalculaJuros(100, 0));
        }
    }
}
